using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{

    private List<Meteors> _Items = new List<Meteors>();
    [SerializeField]  private GameObject _meteor;
    private GameObject _player;
    private float Randx;
    private float Randy;
    Vector2 WereSpawn;
    int i=0;

    void Start()
    {
        _player = GetComponentInChildren<Ship>().gameObject;
    }


    void Update()
    {

        if (_Items.Count <= 70)
        {
            i++;
         
            Randx = Random.Range(200.0f, -200.0f);
            Randy = Random.Range(200.0f, -200.0f);
            WereSpawn = new Vector2(_player.transform.position.x + Randx, _player.transform.position.y + Randy);
            Instantiate(_meteor, gameObject.transform);
        }
        _Items = new List<Meteors>(GetComponentsInChildren<Meteors>());
        if (_Items.Count != 0)
        {
            _Items[_Items.Count - 1].transform.position = WereSpawn;

            if (Vector3.Distance(_Items[_Items.Count - 1].transform.position, _player.transform.position) < 100f)
            {

                Destroy(_Items[_Items.Count - 1].gameObject);
            }
            
        }

    }

        
}
