using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteors : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private float _damage = 1f;

    private HealthManager _hp;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerStay(Collider col)
    {
        if(col.tag == "Player")
        {
            _hp = col.GetComponent<HealthManager>();
            _hp.ChangeHelth(_damage);
        }
    }


    void Update()
    {
        if (Vector3.Distance(transform.position, _player.transform.position) > 300f)
        {
            Destroy(gameObject);
        }
    }
}
