using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _damage = 1f;
    private float _buletspeed = 100f;

    private HealthManager _hp;

    //private int _scoreCount = 1;
    //private ScoreManager _score;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag != "Player")
        {
            Destroy(gameObject);
            _hp = col.GetComponent<HealthManager>();
            _hp.ChangeHelth(_damage);

            //_score.ScoreChange(_scoreCount);
        }
    }

    void Update()
    {

        transform.Translate(Vector2.up * _buletspeed * Time.deltaTime);


    }
}
