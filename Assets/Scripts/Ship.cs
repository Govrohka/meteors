using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField] private AudioClip shot;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject shotdir;
    [SerializeField] private float force = 2;
    private Rigidbody _rigidbody;
    int i = 0;
    float Speed;
    private Vector3 oldPoticion;
    void Start()
    {
        oldPoticion = transform.position;
        _rigidbody = GetComponent<Rigidbody>();

    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            

                Instantiate(bullet.gameObject, shotdir.transform.position, transform.rotation);

                GetComponent<AudioSource>().PlayOneShot(shot);

            
        }

        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.AddRelativeForce(Vector2.up * force);
        }
        if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.AddRelativeForce(Vector2.down * force);
        }
        if (Input.GetKey(KeyCode.D))
        {
            i += 2;
            
        }
        if (Input.GetKey(KeyCode.A))
        {
            i -= 2;
           
        }

        
        if ((i > 0)&&(i != 0))
        {
            i--;
        }

        if ((i < 0) && (i != 0))
        {
            i++;
        }

        transform.Rotate(0, 0, 0.0010f * i);
    }

    void FixedUpdate()
    {
       Speed = Vector3.Distance(transform.position, oldPoticion) / 1.2f * 100;
        oldPoticion = transform.position;
        Debug.Log(Speed);
    }

}
