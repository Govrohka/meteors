using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text _score;

    private int _countS = 0;

    private void Start()
    {
        _score.text = "SCORE: " + _countS;
    }

    public void ScoreChange(int i)
    {
        Debug.Log("SCORE");
        _countS = _countS + i;
    }

}
