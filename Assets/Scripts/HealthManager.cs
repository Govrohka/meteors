using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private float _health = 1f;

    [Header("TEST PRIKOL")]
    [SerializeField] private GameObject _smallMetPref = null;

    public void ChangeHelth(float damage)
    {
        Debug.Log("ChangeHelth");
        _health -= damage;

        if (_health <= 0) Dead();
    }

    private void Dead()
    {
        if (gameObject.tag == "Player")
        {
            gameObject.SetActive(false);
        }
        
        if(gameObject.tag == "Big")
        {
            GameObject smallMet = Instantiate(_smallMetPref);
            GameObject smallMet2 = Instantiate(_smallMetPref);
            smallMet.transform.position = transform.position;
            smallMet2.transform.position = transform.position;
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
